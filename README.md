# CAT - Dossier
This repository is a demo how to upload and display results from an analysis repository to a Dossier.
Here we receive output from the [Continuous Analysis Template](https://gitlab.cern.ch/sneubert/ContinuousAnalysisTemplate/)

*Created from https://gitlab.cern.ch/sneubert/ContinuousAnalysisTemplate/commits/5cfa3e269275e3fb6bc3180f8677bc1edae8723c on branch master*

# Slides
[Slides](latex/CAT.pdf)

# Plots

![Xfit](plots/Xfit.png)