#!/usr/bin/env python

# usage buildDossier.py <workdir> <template in workdir> <outfile>

import sys
workdir=sys.argv[1]
templ=sys.argv[2]
outfile=sys.argv[3]

import jinja2
import git

templateLoader = jinja2.FileSystemLoader( searchpath=workdir )
templateEnv = jinja2.Environment( loader=templateLoader )
TEMPLATE_FILE = templ

template = templateEnv.get_template( TEMPLATE_FILE )

r=git.Repo('')

d=r.git.remote("-v")
remotes=d.split("\n")
origin_fetch=[r.split() for r in remotes if ("origin" in r) and ("fetch" in r)]
url=origin_fetch[0][1];
print(url)
urls=url.split("gitlab.cern.ch")
url="https://gitlab.cern.ch"+urls[1]
url=url.replace(":7999","") # take care of local clones
url=url.replace(".git","")
print(url)
name_rev=r.head.commit.name_rev
rev=name_rev.split()[0]
name=name_rev.split()[1]

templateVars = { "url":url,"rev" : rev , "branch" : name}

outputText = template.render( templateVars )

with open(outfile,"w") as outfile :
    outfile.write(outputText);
